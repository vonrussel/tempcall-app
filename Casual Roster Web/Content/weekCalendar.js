﻿
    function weekCalendar(container, config) {
        config = config || {};

        var dateFormat = config.dateFormat || 'D';
        var currentDate = moment();
        if(config.startDate) {
            currentDate = moment(config.startDate);
        }
        currentDate = currentDate.startOf('isoweek').toDate();


        generateUI();

        // cache jq elements
        var elWeekLabel = container.find('.week-label');
        var elWeekLabelDate = container.find('.week-date-label');
        var elDaysListItems = container.find('.day-item');

        // callbacks
        var onAfterWeekNavigate;


        function getWeekNumber(weekStarts) {
            return moment(weekStarts).week();
        }

        function updateLabels() {
            tmp = moment(currentDate);
            elWeekLabel.html(tmp.format('MMMM YYYY') + ', Week ' + tmp.week());
            elWeekLabelDate.html(tmp.format(dateFormat));
            // update day item labels
            
            elDaysListItems.each(function (i, e) {
                self = $(this);
                self.data('date', tmp.toDate());
                self.find('.day-label').text(tmp.format('dddd D'));
                tmp.add(1, 'days');
                return;
            });
        }

        function generateUI() {
            // generate ui -- call this before updating the labels
            container.append($('.wc-days-header-row-container').html());

            // day rows
            tmp = moment(currentDate);
            
            for (var i = 0; i <= 6; i++) {
                html = $($('.wc-days-items-hidden-container').html());
                tmp = tmp.add(1, 'days');
                container.find('> ul').append(html);
            }
        }

        function resetCheckboxes() {
            container.find('.check-availability').attr('checked', false);
        }


        // event delagates
        container.on('click', '.navigate-week.forward', function () {
            currentDate = moment(currentDate).add(1, 'weeks').toDate();
            resetCheckboxes();
            updateLabels();
            // callbacks
            if(onAfterWeekNavigate) {
                onAfterWeekNavigate();
            }
        });
        container.on('click', '.navigate-week.back', function () {
            currentDate = moment(currentDate).subtract(1, 'weeks').toDate();
            resetCheckboxes();
            updateLabels();
            // callbacks
            if(onAfterWeekNavigate) {
                onAfterWeekNavigate();
            }
        });


        // initial
        updateLabels();


        // to allow external calls to add events for dynamically added elements
        this.addEvent = function (selector, event, fn) {
            container.on(event, selector, fn);
        };

        this.addControlForDay = function (fn) {
            elDaysListItems.find('.controls').each(function (i, e) {
                fn($(this), i)
            });
        };

        this.addControlScheduleForDay = function (fn) {
            elDaysListItems.find('.day-schedule').each(function (i, e) {
                fn($(this), i)
            });
        };

        this.getDateMoment = function() {
            return moment( currentDate );
        };

        this.setTitle = function (title) {
            container.find('.title').html(title);
        };

        this.onAfterWeekNavigate = function(fn) {
            onAfterWeekNavigate = fn;  
        };

        this.container = container;

        return this;

    }