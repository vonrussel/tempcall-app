﻿


$(function() {
    $('#mobile').intlTelInput({
        preferredCountries: ['au', 'ph'],
        utilsScript: "/Content/intltelinput/src/utils.js",
        defaultCountry: $('#mobileCountryCode').val()
    });

    $('.form-submit').click(function() {
        countryCode = $("#mobile").intlTelInput("getSelectedCountryData").iso2.toUpperCase();
        $('#mobileCountryCode').val(countryCode);
        $('.body-container form').submit();
    });
});


