﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Casual_Roster_Web
{
    public abstract class ApplicationViewPage<T> : WebViewPage<T>
    {
        protected override void InitializePage()
        {
            SetViewBagDefaultProperties();
            base.InitializePage();
        }

        private void SetViewBagDefaultProperties()
        {
            try
            {
                ViewBag.CurrentUser = new Casual_Roster.User(Casual_Roster.User.GetUser(emailUsername: System.Web.HttpContext.Current.User.Identity.Name).ID);

            }
            catch (Exception)
            {

            }
        }
    }
}
