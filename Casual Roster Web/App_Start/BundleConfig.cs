﻿using System.Web;
using System.Web.Optimization;
using BundleTransformer.Core.Builders;
using BundleTransformer.Core.Orderers;
using BundleTransformer.Core.Transformers;

namespace Casual_Roster_Web
{

    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js",
                      "~/Content/js/device.min.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/font-awesome.min.css",
                      "~/Content/site.css"));

            bundles.Add(new ScriptBundle("~/bundles/momentjs").Include(
                    "~/Scripts/moment.min.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                    "~/Scripts/jquery.validate*",
                    "~/Scripts/_extensions.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/underscore").Include(
                    "~/Scripts/underscore.min.js"
                ));

            bundles.Add(new ScriptBundle("~/Content/geocomplete").Include(
                   "~/Content/geocomplete/jquery.geocomplete.min.js"
               ));
            
            // select 2
            bundles.Add(new ScriptBundle("~/bundles/select2").Include(
                   "~/Scripts/select2.min.js"
               ));
            bundles.Add(new StyleBundle("~/Content/select2Css").Include(
                   "~/Content/css/select2.css"
               ));

            // intltelinput
            bundles.Add(new ScriptBundle("~/bundles/intltelinput").Include(
                   "~/Content/intltelinput/src/intlTelInput.min.js",
                   "~/Content/intltelinput/src/casual-roster-phone-picker.js"
               ));
            bundles.Add(new StyleBundle("~/bundles/intltelinputCss").Include(
                   "~/Content/intltelinput/src/intlTelInput.css"
               ));






            bundles.Add(new ScriptBundle("~/Content/weekCalendar").Include(
                   "~/Content/weekCalendar.js"
               ));

            bundles.Add(new StyleBundle("~/Content/weekCalendarStyles").Include(
                   "~/Content/weekCalendar.css"
               ));


            var less = new Bundle("~/Content/less").Include(
                    "~/Content/site.less"
                );
            less.Builder = new NullBuilder();
            less.Orderer = new NullOrderer();
            less.Transforms.Add(new StyleTransformer());
            less.Transforms.Add(new CssMinify());
            bundles.Add(less);

        }
    }
}
