﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;
using FluentValidation.Results;

namespace Casual_Roster_Web.Validators
{
    public class TeacherViewValidator : AbstractValidator<Casual_Roster_Web.Models.TeacherViewModel>
    {

        public TeacherViewValidator()
        {
            RuleSet("EditProfile", () =>
            {
                RuleFor(x => x.firstName).NotEmpty();
                RuleFor(x => x.lastName).NotEmpty();
                RuleFor(x => x.email).NotEmpty().EmailAddress();
                RuleFor(x => x.mobile).NotEmpty();
                RuleFor(x => x.postCode).NotEmpty();
                RuleFor(x => x.gender).NotEmpty();
                //RuleFor(x => x.preferredSchools).NotEmpty();

                Custom(x =>
                {
                    bool noLocation = x.lat == null || x.lng == null || x.formattedAddress == null;
                    if (noLocation)
                        return new ValidationFailure("formattedAddress", "Location must have a value");
                    return null;
                });

                Custom(x =>
                {
                    // TODO maybe get the user here? for security. as of now id is just included in the form
                    bool existing = Casual_Roster.User.EmailExists(x.email, x.user_id);
                    if (existing)
                        return new ValidationFailure("email", "Email address is already in use");
                    return null;
                });

                // check if mobile is valid
                Custom(x =>
                {
                    bool valid = Casual_Roster.User.mobileIsValid(x.mobile, x.mobileCountryCode);
                    if (!valid)
                        return new ValidationFailure("mobile", "Mobile number is invalid. Do not include country code");
                    return null;
                });

                // check if mobile exists
                Custom(x =>
                {
                    bool existing = Casual_Roster.User.mobileExists(x.mobile, x.mobileCountryCode, x.user_id);
                    if (existing)
                        return new ValidationFailure("mobile", "Mobile number is already in use.");
                    return null;
                });

            });



            RuleSet("Username", () =>
            {
                RuleFor(x => x.User.username).NotEmpty();
                RuleFor(x => x.User.username)
                    .Matches(@"^(?=.{5,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$")
                    .WithMessage("Username must be in correct format");


                // check if username exists
                Custom(x =>
                {
                    // null validation is handled
                    if (!String.IsNullOrEmpty(x.User.username))
                    {
                        bool existing = Casual_Roster.User.UsernameExists(x.User.username, x.user_id);
                        if (existing)
                        {
                            return new ValidationFailure("username,User.username", "Username is already in use");
                        }
                    }
                    return null;
                });


            });

            RuleSet("Password", () =>
            {
                RuleFor(x => x.User.password).NotEmpty();

            });

            RuleSet("RegisterRequested", () =>
            {
                RuleFor(x => x.firstName).NotEmpty();
                RuleFor(x => x.lastName).NotEmpty();
                RuleFor(x => x.email).NotEmpty().EmailAddress();
                RuleFor(x => x.mobile).NotEmpty();
                RuleFor(x => x.postCode).NotEmpty();
                RuleFor(x => x.gender).NotEmpty();
                //RuleFor(x => x.preferredSchools).NotEmpty();

                Custom(x =>
                {
                    // TODO maybe get the user here? for security. as of now id is just included in the form
                    bool existing = Casual_Roster.User.EmailExists(x.email, x.user_id);
                    if (existing)
                        return new ValidationFailure("email", "Email address is already in use");
                    return null;
                });

                // check if mobile is valid
                Custom(x =>
                {
                    bool valid = Casual_Roster.User.mobileIsValid(x.mobile, x.mobileCountryCode);
                    if (!valid)
                        return new ValidationFailure("mobile", "Mobile number is invalid. Do not include country code.");
                    return null;
                });

                // check if mobile exists
                Custom(x =>
                {
                    bool existing = Casual_Roster.User.mobileExists(x.mobile, x.mobileCountryCode, x.user_id);
                    if (existing)
                        return new ValidationFailure("mobile", "Mobile number is already in use.");
                    return null;
                });

            });

            RuleSet("RegisterTeacher", () =>
            {
                RuleFor(x => x.password_repeat).NotEmpty().WithMessage("Repeat password must not be empty");
                Custom(x =>
                {
                    if (x.password_repeat != x.User.password)
                    {
                        return new ValidationFailure("password_repeat", "Password does not match");
                    }
                    return null;
                });

                // check if mobile is valid
                Custom(x =>
                {
                    bool valid = Casual_Roster.User.mobileIsValid(x.mobile, x.mobileCountryCode);
                    if (!valid)
                        return new ValidationFailure("mobile", "Mobile number is invalid. Do not include country code");
                    return null;
                });

                // check if mobile exists
                Custom(x =>
                {
                    bool existing = Casual_Roster.User.mobileExists(x.mobile, x.mobileCountryCode, x.user_id);
                    if (existing)
                        return new ValidationFailure("mobile", "Mobile number is already in use.");
                    return null;
                });
            });

            RuleSet("RequestTeacher", () =>
            {
                RuleFor(x => x.firstName).NotEmpty();
                RuleFor(x => x.lastName).NotEmpty();
                RuleFor(x => x.email).EmailAddress();
                RuleFor(x => x.mobile).NotEmpty();
                RuleFor(x => x.category).NotEmpty();
                RuleFor(x => x.postCode).NotEmpty();

                Custom(x =>
                {
                    bool existing = Casual_Roster.User.EmailExists(x.email);
                    if (existing)
                        return new ValidationFailure("email", "Email address is already in use");
                    return null;
                });

                // check if mobile is valid
                Custom(x =>
                {
                    bool valid = Casual_Roster.User.mobileIsValid(x.mobile, x.mobileCountryCode);
                    if (!valid)
                        return new ValidationFailure("mobile", "Mobile number is invalid. Do not include country code.");
                    return null;
                });

                // check if mobile exists
                Custom(x =>
                {
                    bool existing = Casual_Roster.User.mobileExists(x.mobile, x.mobileCountryCode);
                    if (existing)
                        return new ValidationFailure("mobile", "Mobile number is already in use.");
                    return null;
                });

                

            });





        }

    }
}
