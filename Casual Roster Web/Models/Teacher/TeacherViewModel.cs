﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Casual_Roster_Web.Models
{


    [FluentValidation.Attributes.Validator(typeof(Validators.TeacherViewValidator))]
    // this model will be used in all teacher forms
    // add necessary fields, just add ruleset to isolate required fields
    public class TeacherViewModel : Casual_Roster.TeacherProfileTable
    {

        // convert view model to table model
        public Casual_Roster.TeacherProfileTable ToProfile()
        {

            Casual_Roster.LocationTable loc = null;
            if(this.lat != null && this.lng != null) {
                loc = new Casual_Roster.LocationTable()
                {
                    lat = (decimal)this.lat,
                    lng = (decimal)this.lng,
                    formattedAddress = this.formattedAddress
                };
            }

            return new Casual_Roster.TeacherProfileTable()
            {
                user_id = this.user_id,
                User = this.User,
                firstName = this.firstName,
                lastName = this.lastName,
                gender = this.gender,
                email = this.email,
                mobile = this.mobile,
                mobileAlter = this.mobileAlter,
                mobileCountryCode = this.mobileCountryCode,
                postCode = this.postCode,
                category = this.category,
                preferredSchools = this.preferredSchools,
                Location = loc
            };
        }

        public void FromProfile(Casual_Roster.TeacherProfileTable profile)
        {
            if (profile == null) return;
            this.user_id = profile.user_id;
            this.User = profile.User;
            this.firstName = profile.firstName;
            this.lastName = profile.lastName;
            this.gender = profile.gender;
            this.email = profile.email;
            this.mobile = profile.mobile;
            this.mobileAlter = profile.mobileAlter;
            this.mobileCountryCode = profile.mobileCountryCode;
            this.postCode = profile.postCode;
            this.category = profile.category;
            this.preferredSchools = profile.preferredSchools;
            this.PhotoData = profile.PhotoData;
            if (profile.Location != null)
            {
                this.lat = profile.Location.lat;
                this.lng = profile.Location.lng;
                this.formattedAddress = profile.Location.formattedAddress;
            }
            
        }


        public decimal? lat { get; set; }
        public decimal? lng { get; set; }

        public string password_repeat { get; set; }

        public string formattedAddress { get; set; }



    }

}
