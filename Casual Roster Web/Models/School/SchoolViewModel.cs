﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casual_Roster;

namespace Casual_Roster_Web.Models
{


    [FluentValidation.Attributes.Validator(typeof(Validators.SchoolViewValidator))]
    // this model will be used in all teacher forms
    // add necessary fields, just add ruleset to isolate required fields
    public class SchoolViewModel : Casual_Roster.SchoolProfileTable
    {

        // convert view model to table model
        public Casual_Roster.SchoolProfileTable ToProfile()
        {
            string _phones = "";
            Casual_Roster.LocationTable location = null;
            UserTable _user = this.User;
            if (this.phones != null)
            {
                _phones = this.phones;
            }
            if (this.lat != null && this.lng != null)
            {
                location = new Casual_Roster.LocationTable()
                {
                    lat = (decimal) this.lat,
                    lng = (decimal) this.lng,
                    formattedAddress = this.formattedAddress
                };
            }

            return new Casual_Roster.SchoolProfileTable()
            {
                User = _user,
                user_id = this.user_id,
                schoolName = this.schoolName,
                email = this.email,
                mobile = this.mobile,
                phones = _phones,
                mobileCountryCode = this.mobileCountryCode,
                postCode = this.postCode,
                LogoData = this.LogoData,
                Location = location
            };
        }

        public void FromProfile(Casual_Roster.SchoolProfileTable profile)
        {
            if (profile == null) return;
            this.user_id = profile.user_id;
            this.schoolName = profile.schoolName;
            this.phones = profile.phones;
            this.email = profile.email;
            this.mobile = profile.mobile;
            this.mobileCountryCode = profile.mobileCountryCode;
            this.postCode = profile.postCode;
            this.LogoData = profile.LogoData;
            if (profile.Location != null)
            {
                this.lat = profile.Location.lat;
                this.lng = profile.Location.lng;
                this.formattedAddress = profile.Location.formattedAddress;
            }
            
        }


        public decimal? lat { get; set; }
        public decimal? lng { get; set; }

        public string password_repeat { get; set; }

        public string formattedAddress { get; set; }



    }

}
