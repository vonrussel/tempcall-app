﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using FluentValidation.Mvc;
using WebGrease.Extensions;

namespace Casual_Roster_Web.Controllers
{
    public class AccountController : Controller
    {
        //
        // GET: /Account/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Login()
        {
            if (Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login");
        }

        [HttpPost]
        public ActionResult Login([CustomizeValidator(RuleSet = "default,LoginWithEmail")] Casual_Roster.UserTable user, string returnUrl)
        {
            // authentication is handled on validator
            if (!ModelState.IsValid)
            {
                return View("Login", user);
            }

            
            // authentication passed
            FormsAuthentication.SetAuthCookie(user.username, false);
            // exec hooks
            Casual_Roster.User.OnLogin(Casual_Roster.User.GetUser(emailUsername: user.username));

            // redirect to where user access first
            if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/")
                && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
            {
                return Redirect(returnUrl);
            }

            return RedirectToAction("Redirect");
        }

        public ActionResult Subscribe()
        {
            return View(new Stripe.StripeCard());
        }

        public ActionResult Redirect()
        {
            if (System.Web.HttpContext.Current.User.IsInRole("teacher"))
            {
                return Redirect(Url.Content("~/Teacher/Availability"));
            }
            else if (System.Web.HttpContext.Current.User.IsInRole("school"))
            {
                return Redirect(Url.Content("~/School/Roster"));
            }
            return HttpNotFound();
        }

	}
}