﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using Casual_Roster;
using Casual_Roster_Web.Models;
using FluentValidation.Mvc;
using Newtonsoft.Json.Linq;

namespace Casual_Roster_Web.Controllers
{
    public class SchoolController : Controller
    {

        public ActionResult Register()
        {
            var school = new Casual_Roster.SchoolProfileTable();
            school.mobileCountryCode = System.Globalization.RegionInfo.CurrentRegion.TwoLetterISORegionName;
            //ViewBag.countryCodes = Casual_Roster.Utils.GetSelectListCountries();
            var schoolModel = new SchoolViewModel();
            schoolModel.FromProfile(school);
            return View(schoolModel);
        }

        
        [HttpPost]
        public ActionResult Register([CustomizeValidator(RuleSet = "Register")] Casual_Roster_Web.Models.SchoolViewModel school, HttpPostedFileBase uploadedLogo)
        {
            if (!ModelState.IsValid)
            {
                return View("Register", school);
            }

            Casual_Roster.School.Register(school.ToProfile());
            var createdSchool = new Casual_Roster.School(school.user_id);
            return RedirectToAction("Roster");
        }


        public ActionResult AddTeacher()
        {
            var user = Casual_Roster.User.GetUser(emailUsername: System.Web.HttpContext.Current.User.Identity.Name);
            var teacher = new Teacher(user.ID);
            return View(new TeacherViewModel()
            {
                mobileCountryCode = teacher.Profile != null ? teacher.Profile.mobileCountryCode : System.Globalization.RegionInfo.CurrentRegion.TwoLetterISORegionName
            });
        }

        
        [HttpPost]
        [Authorize(Roles = "school")]
        public ActionResult AddTeacher([CustomizeValidator(RuleSet = "RequestTeacher")] Models.TeacherViewModel teacher, int teacherId = 0)
        {
            var school = Casual_Roster.User.GetUser(emailUsername: System.Web.HttpContext.Current.User.Identity.Name);
            // mobile is selected
            if (teacherId != 0)
            {
                // selected teacher via mobile is a existing user
                Casual_Roster.School.RequestExistingTeacher(teacherId, school.ID);
                return RedirectToAction("Roster");
                
            } 
            else if (!ModelState.IsValid)
            {
                return View("AddTeacher", teacher);
            }
            
            teacher.mobileCountryCode = System.Globalization.RegionInfo.CurrentRegion.TwoLetterISORegionName;
            Casual_Roster.School.RequestTeacher(teacher.ToProfile(), school.ID);

            return RedirectToAction("Roster");
        }


        [Authorize(Roles = "school")]
        public ActionResult EditProfile()
        {
            // always current user is editing his profile
            var user = Casual_Roster.User.GetUser(emailUsername: System.Web.HttpContext.Current.User.Identity.Name);
            var viewModel = new Models.SchoolViewModel();
            viewModel.FromProfile(new Casual_Roster.School(user.ID).Profile);
            return View(viewModel);
        }

        [HttpPost]
        [Authorize(Roles = "school")]
        public ActionResult EditProfile([CustomizeValidator(RuleSet = "EditProfile")] Models.SchoolViewModel school, HttpPostedFileBase uploadedLogo)
        {

            // always current user is editing his profile
            var user = Casual_Roster.User.GetUser(emailUsername: System.Web.HttpContext.Current.User.Identity.Name);
            var _school = new Casual_Roster.School(user.ID);            

            if (!ModelState.IsValid)
            {
                school.FromProfile(_school.Profile);
                return View(school);
            }

            // udpate the profile
            _school.UpdateProfile(school.ToProfile());
            if (uploadedLogo != null)
            {
                _school.UpdateLogo(uploadedLogo, Server);
            }

            ViewBag.success = true;
            school.FromProfile(_school.Profile);
            return View(school); // TODO improve this not to reinit
        }



        [Authorize(Roles="school")]
        public ActionResult Roster(string startDate)
        {
            ViewBag.date = startDate ?? "";
            return View();
        }


        [Authorize]
        public ActionResult GetStaff(int year, int month, int day)
        {
            DateTime d = new DateTime();

            try
            {
                // if date is invalid
                d = new DateTime(year, month, day);
            } catch(Exception) {
                return HttpNotFound();
            }

            // TODO add method for getting current user
            var user = Casual_Roster.User.GetUser(emailUsername: System.Web.HttpContext.Current.User.Identity.Name);
            ViewBag.schoolId = user.ID;
            ViewBag.requestDate = d;
            // exclude teachers who are already requested
            ViewBag.availableTeachers = (IList<Casual_Roster.Teacher>)Casual_Roster.Teacher.GetAvailableIn(d).ToList();
            //ViewBag.availableTeachers = (IList<Casual_Roster.Teacher>)Casual_Roster.Teacher.GetAll().ToList();
            
            return View();
        }


        [HttpPost]
        public ActionResult RosterRequest(int[] teacherIds, int schoolId, DateTime rosterDate)
        {
            List<String> errors = new List<String>();
            if (teacherIds == null || schoolId == null || rosterDate == null)
            {
                errors.Add("Invalid parameters");
            }


            foreach (var teacherId in teacherIds)
            {
                // todo validate if teacher is available
                var teacher = new Casual_Roster.Teacher(teacherId);
                Casual_Roster.Roster roster = new Casual_Roster.Roster(
                    teacher,
                    new Casual_Roster.School(schoolId),
                    rosterDate
                );

                try
                {
                    roster.Save();
                }
                catch (Exception)
                {
                    errors.Add("You already requested " + teacher.Profile.firstName);
                }
            }

            return Json( new {
                success = errors.Count == 0,
                errors = errors.ToArray()
            } );
        }

        [HttpPost]
        [Authorize]
        public ActionResult GetRosterRequests(int week)
        {
            var user = Casual_Roster.User.GetUser(emailUsername: System.Web.HttpContext.Current.User.Identity.Name);
            var requests = Casual_Roster.Roster.GetRequestsInWeek(week, user.ID);
            List<dynamic> ret = new List<dynamic>();

            // remove recursive data
            foreach (var req in requests)
            {
                ret.Add(
                    new
                    {
                        RosterDateUnix = req.RosterDateUnix,
                        id = req.id,

                        Teacher = new
                        {
                            id = req.Teacher.id,
                            Profile = new
                            {
                                firstName = req.Teacher.Profile.firstName,
                                lastName = req.Teacher.Profile.lastName,
                                photo = (req.Teacher.Profile.PhotoData != null ? req.Teacher.Profile.PhotoData.filename : "")
                            }
                        },

                        School = new {
                            id = req.School.id,
                            Logo = req.School.Logo,
                            Profile = new
                            {
                                schoolName = req.School.Profile.schoolName
                            }
                        },

                        Request = new
                        {
                            ID = req.Request.ID,
                            roster_date = req.Request.roster_date,
                            status = req.Request.status
                        }
                    }
                );
            }

            return Json(new
            {
                //test = System.Globalization.RegionInfo.CurrentRegion.TwoLetterISORegionName,
                requests = ret.ToArray()
            }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult Profile(int id)
        {
            var school = new Casual_Roster.School(id);
            return View(school);
        }

	}
}