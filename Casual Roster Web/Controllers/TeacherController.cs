﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Web;
using System.Web.Mvc;
using Casual_Roster;
using Casual_Roster_Web.Models;
using FluentValidation.Mvc;

namespace Casual_Roster_Web.Controllers
{
    public class TeacherController : Controller
    {

        public ActionResult Register()
        {
            var teacher = new Casual_Roster.TeacherProfileTable();
            var teacherView = new TeacherViewModel();
            teacher.mobileCountryCode = System.Globalization.RegionInfo.CurrentRegion.TwoLetterISORegionName;
            teacherView.FromProfile(teacher);

            ViewBag.categoriesSelectList = new SelectList(
                Teacher.Categories.Select(x =>
                    new KeyValuePair<String, String>(x, x)
                    )
                , "Key", "Value"
            );
            return View(teacherView);
        }


        [HttpPost]
        public ActionResult Register([CustomizeValidator(RuleSet = "default,RegisterTeacher")] TeacherViewModel teacherViewModel, HttpPostedFileBase uploadedLogo)
        {

            if (!ModelState.IsValid)
            {

                // categories selectlist box
                ViewBag.categoriesSelectList = new SelectList(
                    Teacher.Categories.Select(x =>
                        new KeyValuePair<String, String>(x, x)
                        )
                    , "Key", "Value", teacherViewModel.category);
                return View("Register", teacherViewModel);
            }

            teacherViewModel.mobileCountryCode = System.Globalization.RegionInfo.CurrentRegion.TwoLetterISORegionName;
            var teacher = teacherViewModel.ToProfile();
            Casual_Roster.Teacher.Register(teacher);
            if (uploadedLogo != null)
            {
                new Casual_Roster.Teacher(teacher.user_id).UpdatePhoto(uploadedLogo, Server);
            }

            return RedirectToAction("Login", "Account");
        }

        public ActionResult RegisterRequested(int id)
        {
            var teacherData = Casual_Roster.School.GetRequestTeacherData(id);
            if (teacherData != null)
            {
                // if teacher is already registered
                if (teacherData.has_registered)
                {
                    return HttpNotFound();
                }
                var teacher = new Casual_Roster.Teacher(id).Profile;
                teacher.mobileCountryCode = System.Globalization.RegionInfo.CurrentRegion.TwoLetterISORegionName;
                
                // cast to view object
                var viewModel = new Models.TeacherViewModel();
                viewModel.FromProfile(teacher);
                return View(viewModel);
            }
            return HttpNotFound();
            
        }

        [HttpPost]
        public ActionResult RegisterRequested([CustomizeValidator(RuleSet = "Username,Password,RegisterRequested")] Models.TeacherViewModel teacher, int id, HttpPostedFileBase uploadedLogo)
        {
            teacher.user_id = id;
            // if teacher is already registered
            if (Casual_Roster.School.GetRequestTeacherData(id).has_registered)
            {
                return HttpNotFound();
            }

            if (!ModelState.IsValid)
            {
                return View("RegisterRequested", teacher);
            }

            Casual_Roster.Teacher.UpdateRequested(teacher.ToProfile());
            // update photo
            if(uploadedLogo != null) {
                new Casual_Roster.Teacher(teacher.user_id).UpdatePhoto(uploadedLogo, Server);
            }

            return RedirectToAction("Login", "Account");
        }



        #region Avalabilities

        [Authorize(Roles="teacher")]
        public ActionResult Availability(string startDate)
        {
            var emailUsername = System.Web.HttpContext.Current.User.Identity.Name;
            var user = Casual_Roster.User.GetUser(emailUsername: emailUsername);
            ViewBag.userId = user.ID;
            ViewBag.date = startDate ?? "";
            return View();
        }


        [Authorize]
        public ActionResult jsonAvailabilities(int teacherId, int week, int year)
        {
            var teacher = new Casual_Roster.Teacher(teacherId);

            return Json(new
            {
                availabilities = teacher.GetAvailabilities(week, year)
            }, JsonRequestBehavior.AllowGet);
        }


        [Authorize(Roles="teacher")]
        [HttpPost]
        public ActionResult SaveAvailabilities(int[] daysAvailable, int week, int year)
        {
            var user = Casual_Roster.User.GetUser(emailUsername: System.Web.HttpContext.Current.User.Identity.Name);
            var teacher = new Casual_Roster.Teacher(user.ID);
            teacher.SaveAvailabilities(daysAvailable, week, year);
            return Json(new {  });
        }

        #endregion


        [HttpPost]
        [Authorize]
        public ActionResult GetRosterRequests(DateTime date)
        {
            var user = Casual_Roster.User.GetUser(emailUsername: System.Web.HttpContext.Current.User.Identity.Name);
            return Json(new
            {
                requests = Casual_Roster.Roster.GetRequestsForDay(date, teacherId: user.ID).ToArray()
            }, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles="teacher")]
        public ActionResult RosterAction(int rosterId) 
        {
            var roster = new Casual_Roster.Roster(rosterId);
            if (Casual_Roster.User.GetUser(emailUsername: System.Web.HttpContext.Current.User.Identity.Name).ID != roster.Teacher.id)
            {
                return HttpNotFound();
            }
            ViewBag.roster = roster;
            return View();
        }

        [Authorize(Roles = "teacher")]
        public ActionResult AcceptRoster(int id)
        {
            // TODO check if current teacher is what on the roster request
            new Casual_Roster.Roster(id).UpdateStatus(Casual_Roster.Roster.Statuses.Approved);
            return RedirectToAction("Availability");
        }

        [Authorize(Roles = "teacher")]
        public ActionResult DeclineRoster(int id)
        {
            // TODO check if current teacher is what on the roster request
            new Casual_Roster.Roster(id).UpdateStatus(Casual_Roster.Roster.Statuses.Declined);
            return RedirectToAction("Availability");
        }


        public ActionResult Profile(int id)
        {
            return View(new Casual_Roster.Teacher(id));
        }


        [Authorize(Roles="teacher")]
        public ActionResult EditProfile()
        {
            // always current user is editing his profile
            var user = Casual_Roster.User.GetUser(emailUsername: System.Web.HttpContext.Current.User.Identity.Name);
            var viewModel = new Models.TeacherViewModel();
            viewModel.FromProfile( new Casual_Roster.Teacher(user.ID).Profile );
            return View(viewModel);
        }

        [HttpPost]
        [Authorize(Roles = "teacher")]
        public ActionResult EditProfile([CustomizeValidator(RuleSet = "EditProfile")] Models.TeacherViewModel teacher, HttpPostedFileBase uploadedLogo)
        {
            // always current user is editing his profile
            var user = Casual_Roster.User.GetUser(emailUsername: System.Web.HttpContext.Current.User.Identity.Name);
            var _teacher = new Casual_Roster.Teacher(user.ID);

            if (!ModelState.IsValid)
            {
                teacher.FromProfile(_teacher.Profile);
                return View(teacher);
            }

            // udpate the profile
            _teacher.UpdateProfile(teacher.ToProfile());
            if (uploadedLogo != null)
            {
                _teacher.UpdatePhoto(uploadedLogo, Server);
            }

            ViewBag.success = true;
            teacher.FromProfile(_teacher.Profile);
            return View(teacher); // TODO improve this not to reinit
        }


        [Authorize(Roles="teacher")]
        public ActionResult jsonGetSchoolsAvailable(string ids = "")
        {
            var user = Casual_Roster.User.GetUser(emailUsername: System.Web.HttpContext.Current.User.Identity.Name);
            var schools = Casual_Roster.School.GetAvailableSchoolsForTeacher(user.ID)
                .Where(t =>
                {
                    // for initselection of select2
                    // ids param is passed
                    if (ids != "")
                    {
                        return ids.Split(',').Contains(t.id + "");
                    }

                    return true;
                })
                // hide not required fields
                .Select(t =>
                {
                    var profile = t.Profile;
                    return new
                    {
                        id = t.id,
                        schoolName = profile.schoolName
                    };
                });

            return Json(new
            {
                schools = schools
            }, JsonRequestBehavior.AllowGet);
        }


        [Authorize]
        public ActionResult jsonGetMobileInfo(string mobile, string countryCode)
        {
            var teacher = Casual_Roster.Teacher.GetByMobile(mobile, countryCode);

            if (teacher == null)
            {
                return HttpNotFound();
            }

            return Json(new
            {
                teacher = new
                {
                    user_id = teacher.user_id,
                    mobile = teacher.mobile,
                    firstName = teacher.firstName,
                    lastName = teacher.lastName,
                    email = teacher.email,
                    postCode = teacher.postCode,
                    category = teacher.category
                }
            }, JsonRequestBehavior.AllowGet);
        }




    }
}