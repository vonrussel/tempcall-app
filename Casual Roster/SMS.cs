﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Twilio;

namespace Casual_Roster
{

    public class SMS
    {
        bool testing = false;
        string TWILIO_SID = "ACb47d214805e3ffb12ade31f0509e255f";
        string TWILIO_AUTH_TOKEN = "a80e5faeaaaa21f32be59bb98d6de680";
        string from = "+14695073547";
        string to;


        public SMS(string to, string from = "")
        {
            this.to = to;
            if (!String.IsNullOrEmpty(from))
            {
                this.from = from;
            }
            
        }

        public void Send(string message)
        {
            if(testing) {
                to = "+63 917 5501 409";
            }
            message = "Casual Roster Alert:" + Environment.NewLine + message;
            var sms = this.GetTwilio().SendMessage(from, to, message);
        }


        private TwilioRestClient GetTwilio() {
            return new TwilioRestClient(TWILIO_SID, TWILIO_AUTH_TOKEN);
        }


    }


}
