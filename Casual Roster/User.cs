﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PhoneNumbers; // libphonenumber

namespace Casual_Roster
{


    
    public class User : UserTable
    {

        int id;


        public SchoolProfileTable SchoolProfile
        {
            get
            {
                return new School(id).Profile;
            }
        }

        public TeacherProfileTable TeacherProfile
        {
            get
            {
                return new Teacher(id).Profile;
            }
        }


        public User() { }
        public User(int id)
        {
            this.id = id;
        }


        public bool IsInRole(Roles.Option role)
        {
            return this.Data().roles.Split(';').Contains(role.ToString());
        }

        public static void OnLogin(UserTable user)
        {
            // payments: check if user has a customer profile in stripe
            if (String.IsNullOrEmpty(user.stripeCustomerId))
            {
                Payments.RegisterCustomer(user);
            }
        }

        public UserTable Data() {
            var db = App.Database;
            return db.Users.FirstOrDefault(u => u.ID == this.id);
        }

        public Object GetProfile()
        {
            if (IsInRole(Roles.Option.teacher))
            {
                return App.Database.TeacherProfiles.FirstOrDefault(u => this.id == u.user_id);
            }
            else if (IsInRole(Roles.Option.school))
            {
                return App.Database.SchoolProfiles.FirstOrDefault(u => this.id == u.user_id);
            }
            return null;
        }

        public static UserTable GetUser(string username = "", string email = "", int id = 0, string emailUsername = "")
        {
            var db = App.Database;
            if (!String.IsNullOrEmpty(email))
            {
                // find through schools
                var school = db.SchoolProfiles.FirstOrDefault(s => s.email == email);
                if (school != null)
                {
                    id = school.user_id;
                }

                // find through teachers
                var teacher = db.TeacherProfiles.FirstOrDefault(s => s.email == email);
                if (teacher != null)
                {
                    id = teacher.user_id;
                }
            }

            if (!String.IsNullOrEmpty(emailUsername))
            {
                var school = db.SchoolProfiles.FirstOrDefault(s => s.email == emailUsername);
                if (school != null)
                {
                    id = school.user_id;
                }

                var teacher = db.TeacherProfiles.FirstOrDefault(s => s.User.username == emailUsername || s.email == emailUsername);
                if (teacher != null)
                {
                    id = teacher.user_id;
                }
            }


            return db.Users.FirstOrDefault(u => ((!String.IsNullOrEmpty(username) && u.username == username) || false) || u.ID == id);
        }

        public static bool Auth(string username, string password)
        {
            username = username ?? "";
            password = password ?? "";

            var db =  App.Database;
            var user = User.GetUser(emailUsername: username);

            if (user != null)
            {
                return xx.Security.PasswordHasher.Hash(password, user.password_salt) == user.password;
            }

            return false;
        }

        public static User Current()
        {
            return new User(User.GetUser(emailUsername: System.Web.HttpContext.Current.User.Identity.Name).ID);
        }


        public static bool UsernameExists(string username, int existingId = 0)
        {
            var db = App.Database;
            var existing = db.Users.FirstOrDefault(u =>
                (username == u.username && existingId == 0) ||
                (username == u.username && u.ID != existingId && existingId > 0)  // for exiting user
            );
            return existing != null;
        }

        public static bool mobileExists(string mobile, string countryCode, int existingId = 0)
        {
            mobile = mobile ?? "";
            mobile = Utils.FormatMobileNumber(mobile, countryCode); // convert to national
            mobile = mobile.Trim().Replace(" ", "");

            // validate phone numbers together with country code
            var db = App.Database;
            var existingInSchools = db.SchoolProfiles.FirstOrDefault(u =>
                (mobile == u.mobile.Trim().Replace(" ", "") && countryCode == u.mobileCountryCode && existingId == 0) ||
                (mobile == u.mobile.Trim().Replace(" ", "") && countryCode == u.mobileCountryCode && u.user_id != existingId && existingId > 0)  // for exiting user
            );
            var existingInTeachers = db.TeacherProfiles.FirstOrDefault(u =>
                (mobile == u.mobile.Trim().Replace(" ", "") && countryCode == u.mobileCountryCode && existingId == 0) ||
                (mobile == u.mobile.Trim().Replace(" ", "") && countryCode == u.mobileCountryCode && u.user_id != existingId && existingId > 0)  // for exiting user
            );
            return existingInSchools != null || existingInTeachers != null;
        }

        public static bool mobileIsValid(string mobile, string countryCode)
        {
            PhoneNumber number;
            var phoneUtil = PhoneNumberUtil.GetInstance();

            try
            {
                number = phoneUtil.Parse(mobile, countryCode);
            }
            catch (NumberParseException) {
                return false;
            }

            // do not allow numbers with country code
            if (mobile.Contains("+" + number.CountryCode))
            {
                return false;
            }

            return phoneUtil.IsValidNumber(number);
        }


        public static bool EmailExists(string email, int existingId = 0)
        {
            // check through school and teacher profile for email address
            var db = App.Database;
            var existingInSchools = db.SchoolProfiles.FirstOrDefault(x =>
                (email == x.email && existingId == 0) ||
                (email == x.email && x.user_id != existingId && existingId > 0)  // for exiting user
            );
            var existingInTeachers = db.TeacherProfiles.FirstOrDefault(x =>
                (email == x.email && existingId == 0) ||
                (email == x.email && x.user_id != existingId && existingId > 0)  // for exiting user
            );
            return (existingInSchools != null) || (existingInTeachers != null);
        }


        public static void Save()
        {
            
        }

        // find a way to use this for all fields
        public static void UpdateStripeCustomerId(int userId, string customerId)
        {
            using (var db = App.Database)
            {
                var user = db.Users.FirstOrDefault(u => u.ID == userId);
                if (user != null)
                {
                    user.stripeCustomerId = customerId;
                    db.SaveChanges();
                }
            }
        }




        // TODO, for now this is for add only
        /*
        public int Save()
        {
            this.HashPassword(20);
            var db = App.Database;
            db.Users.Add((UserTable)this);
            // add school profile
            if (this.SchoolProfile != null)
            {
                db.SchoolProfiles.Add(this.SchoolProfile);
            }

            // TODO
            // add username?
            db.SaveChanges();
            return 1;
        }*/

        // TODO add update func, 
        // dont allow similar username 
















    }
}
