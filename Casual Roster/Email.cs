﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using email = xx.Email;

namespace Casual_Roster
{
    public class Email
    {


        #region Email function
        private static string SYSTEM_NAME = "Tempcall";
        private static string SYSTEM_EMAIL = "";
        private static string SYSTEM_EMAIL_PASSWORD = "";

        public static email GetEmailInstance()
        {
            return new email(email.Gmail(SYSTEM_EMAIL, SYSTEM_EMAIL_PASSWORD));
        }

        public static MailAddress SystemSender()
        {
            return new MailAddress(SYSTEM_EMAIL, SYSTEM_NAME);
        }
        #endregion


        public static void SendWelcomeEmail(MailAddress newUserEmail)
        {
            GetEmailInstance().Send(new MailMessage()
            {
                From = SystemSender(),
                To = { newUserEmail },
                Body = "Welcome to Tempcall app!" // TODO: add html templating
            });
        }



    }
}
