﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Casual_Roster
{
    public class Location
    {

        int id;

        public Location(int id)
        {
            this.id = id;
        }

        public LocationTable Data
        {
            get
            {
                var db = App.Database;
                return db.Locations.FirstOrDefault(l => l.ID == this.id);
            }
        }


    }
}
