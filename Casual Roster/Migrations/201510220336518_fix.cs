namespace Casual_Roster.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fix : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "stripeCustomerId", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Users", "stripeCustomerId");
        }
    }
}
