// <auto-generated />
namespace Casual_Roster.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class addphotototeacher : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(addphotototeacher));
        
        string IMigrationMetadata.Id
        {
            get { return "201505250447331_add-photo-to-teacher"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
