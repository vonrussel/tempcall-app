namespace Casual_Roster.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addlocationtoschoolprofiole : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SchoolProfiles", "location_id", c => c.Int());
            CreateIndex("dbo.SchoolProfiles", "location_id");
            AddForeignKey("dbo.SchoolProfiles", "location_id", "dbo.Locations", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SchoolProfiles", "location_id", "dbo.Locations");
            DropIndex("dbo.SchoolProfiles", new[] { "location_id" });
            DropColumn("dbo.SchoolProfiles", "location_id");
        }
    }
}
