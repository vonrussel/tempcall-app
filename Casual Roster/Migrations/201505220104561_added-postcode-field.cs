namespace Casual_Roster.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedpostcodefield : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SchoolProfiles", "postCode", c => c.String());
            AddColumn("dbo.TeacherProfiles", "postCode", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.TeacherProfiles", "postCode");
            DropColumn("dbo.SchoolProfiles", "postCode");
        }
    }
}
