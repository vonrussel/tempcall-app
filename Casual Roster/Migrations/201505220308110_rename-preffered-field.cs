namespace Casual_Roster.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class renameprefferedfield : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TeacherProfiles", "preferredSchools", c => c.String());
            DropColumn("dbo.TeacherProfiles", "prefferedSchools");
        }
        
        public override void Down()
        {
            AddColumn("dbo.TeacherProfiles", "prefferedSchools", c => c.String());
            DropColumn("dbo.TeacherProfiles", "preferredSchools");
        }
    }
}
