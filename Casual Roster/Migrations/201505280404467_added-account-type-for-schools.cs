namespace Casual_Roster.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedaccounttypeforschools : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SchoolProfiles", "accountType", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.SchoolProfiles", "accountType");
        }
    }
}
