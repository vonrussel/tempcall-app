namespace Casual_Roster.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class negateteacheravailability : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TeacherAvailabilities", "daysNotAvailable", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.TeacherAvailabilities", "daysNotAvailable");
        }
    }
}
