namespace Casual_Roster.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Images",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        path = c.String(),
                        originalFilename = c.String(),
                        filename = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.RoasterRequests",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        roster_date = c.DateTime(nullable: false),
                        status = c.String(),
                        created = c.DateTime(nullable: false),
                        teacher_id = c.Int(nullable: false),
                        school_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.SchoolProfiles", t => t.school_id, cascadeDelete: true)
                .ForeignKey("dbo.TeacherProfiles", t => t.teacher_id, cascadeDelete: true)
                .Index(t => t.teacher_id)
                .Index(t => t.school_id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        roles = c.String(),
                        username = c.String(),
                        password = c.String(),
                        password_salt = c.String(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                        RosterRequest_ID = c.Int(),
                        RosterRequest_ID1 = c.Int(),
                        SchoolTeacherRequest_ID = c.Int(),
                        SchoolTeacherRequest_ID1 = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.RoasterRequests", t => t.RosterRequest_ID)
                .ForeignKey("dbo.RoasterRequests", t => t.RosterRequest_ID1)
                .ForeignKey("dbo.SchoolTeacherRequests", t => t.SchoolTeacherRequest_ID)
                .ForeignKey("dbo.SchoolTeacherRequests", t => t.SchoolTeacherRequest_ID1)
                .Index(t => t.RosterRequest_ID)
                .Index(t => t.RosterRequest_ID1)
                .Index(t => t.SchoolTeacherRequest_ID)
                .Index(t => t.SchoolTeacherRequest_ID1);
            
            CreateTable(
                "dbo.SchoolProfiles",
                c => new
                    {
                        user_id = c.Int(nullable: false),
                        schoolName = c.String(),
                        address = c.String(),
                        phones = c.String(),
                        mobile = c.String(),
                        mobileCountryCode = c.String(),
                        email = c.String(),
                        logo = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.user_id)
                .ForeignKey("dbo.Images", t => t.logo, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.user_id)
                .Index(t => t.user_id)
                .Index(t => t.logo);
            
            CreateTable(
                "dbo.SchoolTeacherRequests",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        created = c.DateTime(nullable: false),
                        has_registered = c.Boolean(nullable: false),
                        teacher_id = c.Int(nullable: false),
                        school_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.SchoolProfiles", t => t.school_id, cascadeDelete: true)
                .ForeignKey("dbo.TeacherProfiles", t => t.teacher_id, cascadeDelete: true)
                .Index(t => t.teacher_id)
                .Index(t => t.school_id);
            
            CreateTable(
                "dbo.TeacherAvailabilities",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        teacher_id = c.Int(nullable: false),
                        daysAvailable = c.String(),
                        week = c.Int(nullable: false),
                        year = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.TeacherProfiles", t => t.teacher_id, cascadeDelete: true)
                .Index(t => t.teacher_id);
            
            CreateTable(
                "dbo.TeacherProfiles",
                c => new
                    {
                        user_id = c.Int(nullable: false),
                        firstName = c.String(),
                        lastName = c.String(),
                        email = c.String(),
                        mobile = c.String(),
                        mobileCountryCode = c.String(),
                        mobileAlter = c.String(),
                        category = c.String(),
                    })
                .PrimaryKey(t => t.user_id)
                .ForeignKey("dbo.Users", t => t.user_id)
                .Index(t => t.user_id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TeacherAvailabilities", "teacher_id", "dbo.TeacherProfiles");
            DropForeignKey("dbo.TeacherProfiles", "user_id", "dbo.Users");
            DropForeignKey("dbo.SchoolTeacherRequests", "teacher_id", "dbo.TeacherProfiles");
            DropForeignKey("dbo.RoasterRequests", "teacher_id", "dbo.TeacherProfiles");
            DropForeignKey("dbo.SchoolProfiles", "user_id", "dbo.Users");
            DropForeignKey("dbo.SchoolTeacherRequests", "school_id", "dbo.SchoolProfiles");
            DropForeignKey("dbo.Users", "SchoolTeacherRequest_ID1", "dbo.SchoolTeacherRequests");
            DropForeignKey("dbo.Users", "SchoolTeacherRequest_ID", "dbo.SchoolTeacherRequests");
            DropForeignKey("dbo.RoasterRequests", "school_id", "dbo.SchoolProfiles");
            DropForeignKey("dbo.SchoolProfiles", "logo", "dbo.Images");
            DropForeignKey("dbo.Users", "RosterRequest_ID1", "dbo.RoasterRequests");
            DropForeignKey("dbo.Users", "RosterRequest_ID", "dbo.RoasterRequests");
            DropIndex("dbo.TeacherProfiles", new[] { "user_id" });
            DropIndex("dbo.TeacherAvailabilities", new[] { "teacher_id" });
            DropIndex("dbo.SchoolTeacherRequests", new[] { "school_id" });
            DropIndex("dbo.SchoolTeacherRequests", new[] { "teacher_id" });
            DropIndex("dbo.SchoolProfiles", new[] { "logo" });
            DropIndex("dbo.SchoolProfiles", new[] { "user_id" });
            DropIndex("dbo.Users", new[] { "SchoolTeacherRequest_ID1" });
            DropIndex("dbo.Users", new[] { "SchoolTeacherRequest_ID" });
            DropIndex("dbo.Users", new[] { "RosterRequest_ID1" });
            DropIndex("dbo.Users", new[] { "RosterRequest_ID" });
            DropIndex("dbo.RoasterRequests", new[] { "school_id" });
            DropIndex("dbo.RoasterRequests", new[] { "teacher_id" });
            DropTable("dbo.TeacherProfiles");
            DropTable("dbo.TeacherAvailabilities");
            DropTable("dbo.SchoolTeacherRequests");
            DropTable("dbo.SchoolProfiles");
            DropTable("dbo.Users");
            DropTable("dbo.RoasterRequests");
            DropTable("dbo.Images");
        }
    }
}
