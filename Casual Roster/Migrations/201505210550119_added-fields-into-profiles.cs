namespace Casual_Roster.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedfieldsintoprofiles : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SchoolProfiles", "contactPerson", c => c.String());
            AddColumn("dbo.TeacherProfiles", "gender", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.TeacherProfiles", "gender");
            DropColumn("dbo.SchoolProfiles", "contactPerson");
        }
    }
}
