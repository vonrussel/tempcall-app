namespace Casual_Roster.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class migfix : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Locations", "lat", c => c.Decimal(nullable: false, precision: 8, scale: 5));
            AlterColumn("dbo.Locations", "lng", c => c.Decimal(nullable: false, precision: 8, scale: 5));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Locations", "lng", c => c.Decimal(nullable: false, precision: 10, scale: 10));
            AlterColumn("dbo.Locations", "lat", c => c.Decimal(nullable: false, precision: 10, scale: 10));
        }
    }
}
