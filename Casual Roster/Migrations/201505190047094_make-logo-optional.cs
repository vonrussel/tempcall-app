namespace Casual_Roster.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class makelogooptional : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.SchoolProfiles", "logo", "dbo.Images");
            DropIndex("dbo.SchoolProfiles", new[] { "logo" });
            AlterColumn("dbo.SchoolProfiles", "logo", c => c.Int());
            CreateIndex("dbo.SchoolProfiles", "logo");
            AddForeignKey("dbo.SchoolProfiles", "logo", "dbo.Images", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SchoolProfiles", "logo", "dbo.Images");
            DropIndex("dbo.SchoolProfiles", new[] { "logo" });
            AlterColumn("dbo.SchoolProfiles", "logo", c => c.Int(nullable: false));
            CreateIndex("dbo.SchoolProfiles", "logo");
            AddForeignKey("dbo.SchoolProfiles", "logo", "dbo.Images", "ID", cascadeDelete: true);
        }
    }
}
