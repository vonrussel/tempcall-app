namespace Casual_Roster.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedlocationtabletoteachers : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Locations",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        lat = c.Decimal(nullable: false, precision: 18, scale: 2),
                        lng = c.Decimal(nullable: false, precision: 18, scale: 2),
                        formattedAddress = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.TeacherProfiles", "location_id", c => c.Int());
            CreateIndex("dbo.TeacherProfiles", "location_id");
            AddForeignKey("dbo.TeacherProfiles", "location_id", "dbo.Locations", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TeacherProfiles", "location_id", "dbo.Locations");
            DropIndex("dbo.TeacherProfiles", new[] { "location_id" });
            DropColumn("dbo.TeacherProfiles", "location_id");
            DropTable("dbo.Locations");
        }
    }
}
