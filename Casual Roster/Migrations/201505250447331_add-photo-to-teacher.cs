namespace Casual_Roster.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addphotototeacher : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TeacherProfiles", "photo", c => c.Int());
            CreateIndex("dbo.TeacherProfiles", "photo");
            AddForeignKey("dbo.TeacherProfiles", "photo", "dbo.Images", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TeacherProfiles", "photo", "dbo.Images");
            DropIndex("dbo.TeacherProfiles", new[] { "photo" });
            DropColumn("dbo.TeacherProfiles", "photo");
        }
    }
}
