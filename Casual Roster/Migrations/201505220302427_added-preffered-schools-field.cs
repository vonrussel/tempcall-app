namespace Casual_Roster.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedprefferedschoolsfield : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TeacherProfiles", "prefferedSchools", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.TeacherProfiles", "prefferedSchools");
        }
    }
}
