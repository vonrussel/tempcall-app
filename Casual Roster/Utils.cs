﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.Net;
using System.IO;
using Newtonsoft.Json.Linq;
using System.Web.Mvc;
using PhoneNumbers;

namespace Casual_Roster
{
    public class Utils
    {

        public static DateTime FirstDateOfWeekISO8601(int year, int weekOfYear)
        {
            DateTime jan1 = new DateTime(year, 1, 1);
            int daysOffset = DayOfWeek.Thursday - jan1.DayOfWeek;

            DateTime firstThursday = jan1.AddDays(daysOffset);
            var cal = CultureInfo.CurrentCulture.Calendar;
            int firstWeek = cal.GetWeekOfYear(firstThursday, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

            var weekNum = weekOfYear;
            if (firstWeek <= 1)
            {
                weekNum -= 1;
            }
            var result = firstThursday.AddDays(weekNum * 7);
            return result.AddDays(-3);
        }


        // countries.json should be in ~/Content/
        public static dynamic GetAllCountries(string pathOfJson)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(pathOfJson);
            httpWebRequest.ContentType = "text/json";
            httpWebRequest.Method = "GET";

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var responseText = streamReader.ReadToEnd();
                try
                {
                    return JObject.Parse(responseText);
                }
                catch (Exception)
                {
                    return JArray.Parse(responseText);
                }
                
            }
        }

        public static IEnumerable<SelectListItem> GetSelectListCountries()
        {
            var jArrayCountryCodes = Casual_Roster.Utils.GetAllCountries(Casual_Roster.Utils.AbsoluteUrl("~/Content/countries.json"));
            foreach (var country in jArrayCountryCodes)
            {
                yield return new SelectListItem()
                {
                    Text = country["name"],
                    Value = country["alpha-2"]
                };
            }
        }


        public static string AbsoluteUrl(string path)
        {
            return ResolveServerUrl(System.Web.VirtualPathUtility.ToAbsolute(path));
        }

        public static string ResolveServerUrl(string serverUrl, bool forceHttps = false)
        {
            if (serverUrl.IndexOf("://") > -1)
                return serverUrl;

            string newUrl = serverUrl;
            Uri originalUri = System.Web.HttpContext.Current.Request.Url;
            newUrl = (forceHttps ? "https" : originalUri.Scheme) +
                "://" + originalUri.Authority + newUrl;
            return newUrl;
        }



        public static String FormatMobileNumber(string phone, string countryCode, PhoneNumberFormat format = PhoneNumberFormat.NATIONAL)
        {
            try
            {
                var util = PhoneNumberUtil.GetInstance();
                return util.Format(util.Parse(phone, countryCode), format);
            }
            catch (NumberParseException) { }
            return "";
        }

        

    }
}
