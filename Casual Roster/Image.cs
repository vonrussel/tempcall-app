﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Casual_Roster
{
    public class Image
    {

        int id;
        public HttpPostedFileBase file { get; set; }


        public Image(HttpPostedFileBase file)
        {
            this.file = file;
        }

        public Image(int id)
        {
            this.id = id;
        }

        public ImageTable Data { 
            get 
            {
                var db = App.Database;
                return db.Images.FirstOrDefault(i => i.ID == this.id);
            } 
        }


        public ImageTable Save(HttpServerUtilityBase server)
        {
            // TODO, dont allow png
            var fileName = xx.String_.GenerateRandom(20);
            var extension = Path.GetExtension(this.file.FileName).ToLower();

            // store the file inside ~/App_Data/uploads folder
            var path = Path.Combine(server.MapPath("~/Content/uploads"), fileName + extension);
            try
            {
                file.SaveAs(path);
                // save file info to db
                var db = App.Database;
                var image = new ImageTable()
                {
                    path = path,
                    filename = fileName + extension,
                    originalFilename = this.file.FileName
                };

                db.Images.Add(image);
                db.SaveChanges();
                return image;

            } catch(Exception ex) {
                Console.Write(ex.ToString());

            }

            return null;
            
        }

    }
}
