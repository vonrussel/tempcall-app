﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Casual_Roster
{

    /// <summary>
    /// Table for requests of schools to teacher to register
    /// </summary>
    [Table("SchoolTeacherRequests")]
    [FluentValidation.Attributes.Validator(typeof(Casual_Roster.Validators.UserValidator))]
    public class SchoolTeacherRequest
    {


        [Key]
        public int ID { get; set; }

        public DateTime created { get; set; }

        public bool has_registered { get; set; }

        public int teacher_id { get; set; }
        public virtual ICollection<UserTable> Teacher { get; set; }

        public int school_id { get; set; }
        public virtual ICollection<UserTable> Schools { get; set; }


    }
}
    