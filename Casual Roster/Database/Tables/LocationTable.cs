﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Casual_Roster
{


    [Table("Locations")]
    //[FluentValidation.Attributes.Validator(typeof(Casual_Roster.Validators.TeacherValidator))]
    public class LocationTable
    {

        [Key]
        public int ID { get; set; }

        public decimal lat { get; set; }
        public decimal lng { get; set; }
        public string formattedAddress { get; set; }



    }

}
