﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Casual_Roster
{


    [Table("Images")]
    //[FluentValidation.Attributes.Validator(typeof(Casual_Roster.Validators.TeacherValidator))]
    public class ImageTable
    {

        [Key]
        public int ID { get; set; }

        public string path { get; set; }

        public string originalFilename { get; set; }
        public string filename { get; set; }


        // TODO: add attribs


    }

}
