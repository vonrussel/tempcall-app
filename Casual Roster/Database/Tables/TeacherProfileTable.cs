﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Casual_Roster
{


    [Table("TeacherProfiles")]
    [FluentValidation.Attributes.Validator(typeof(Casual_Roster.Validators.TeacherValidator))]
    public class TeacherProfileTable
    {

        [Display(Name="First name")]
        public string firstName { get; set; }

        [Display(Name = "Last name")]
        public string lastName { get; set; }

        [Display(Name = "Gender")]
        public string gender { get; set; }

        [Display(Name = "Email address")]
        public string email { get; set; }

        [Display(Name = "Mobile number")]
        public string mobile { get; set; }

        [Display(Name = "Mobile number Country Code")]
        public string mobileCountryCode { get; set; }

        [Display(Name = "Alternate mobile number")]
        public string mobileAlter { get; set; }

        [Key]
        public int user_id { get; set; }
        [ForeignKey("user_id")]
        public virtual UserTable User { get; set; }

        [Display(Name = "Type of Temp")]
        public string category { get; set; }

        [Display(Name = "Post code")]
        public string postCode { get; set; }

        [Display(Name = "Preferred Employers")]
        public string preferredSchools { get; set; }

        public int? location_id { get; set; }
        [ForeignKey("location_id")]
        public virtual LocationTable Location { get; set; }


        [Display(Name = "Photo")]
        public int? photo { get; set; }
        [ForeignKey("photo")]
        public virtual ImageTable PhotoData { get; set; }


        public virtual ICollection<RosterRequest> RosterRequests { get; set; }
        public virtual ICollection<SchoolTeacherRequest> SchoolTeacherRequests { get; set; }


    }

}
