﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace Casual_Roster
{

    [Table("Users")]
    [FluentValidation.Attributes.Validator(typeof(Casual_Roster.Validators.UserValidator))]
    public class UserTable : xx.BaseUser
    {
        public int ID { get; set; }
        
        // separated with ;
        public string roles { get; set; }

        // username & password is on the base

        public string stripeCustomerId { get; set; }


    }
}
