﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web;

namespace Casual_Roster
{


    [Table("SchoolProfiles")]
    [FluentValidation.Attributes.Validator(typeof(Casual_Roster.Validators.SchoolValidator))]
    public class SchoolProfileTable
    {

        [Key, ForeignKey("User")]
        public int user_id { get; set; }
        public virtual UserTable User { get; set; }


        [Display(Name="Employer Name")]
        public string schoolName { get; set; }

        [Display(Name = "Contact person")]
        public string contactPerson { get; set; }

        [Display(Name = "Address")]
        public string address { get; set; }

        [Display(Name = "Phone number(s)")]
        public string phones { get; set; }

        [Display(Name = "Mobile number")]
        public string mobile { get; set; }

        [Display(Name = "Mobile number Country Code")]
        public string mobileCountryCode { get; set; }

        [Display(Name = "Email Address")]
        public string email { get; set; }


        [Display(Name = "Logo")]
        public int? logo { get; set; }
        [ForeignKey("logo")]
        public virtual ImageTable LogoData { get; set; }
        [Display(Name = "Post code")]
        public string postCode { get; set; }

        public int? location_id { get; set; }
        [ForeignKey("location_id")]
        public virtual LocationTable Location { get; set; }

        public string accountType { get; set; }


        public virtual ICollection<RosterRequest> RosterRequests { get; set; }
        public virtual ICollection<SchoolTeacherRequest> SchoolTeacherRequests { get; set; }


        // ....
    }
}
