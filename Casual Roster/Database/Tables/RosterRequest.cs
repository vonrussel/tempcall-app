﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Casual_Roster
{

    [Table("RoasterRequests")]
    //[FluentValidation.Attributes.Validator(typeof(Casual_Roster.Validators.UserValidator))]
    public class RosterRequest
    {


        [Key]
        public int ID { get; set; }

        public DateTime roster_date { get; set; }

        public string status { get; set; }

        public DateTime created { get; set; }




        public int teacher_id { get; set; }
        public virtual ICollection<UserTable> Teacher { get; set; }

        public int school_id { get; set; }
        public virtual ICollection<UserTable> Schools { get; set; }

    }
}
    