﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Casual_Roster
{

    [Table("TeacherAvailabilities")]
    //[FluentValidation.Attributes.Validator(typeof(Casual_Roster.Validators.UserValidator))]
    public class TeacherAvailability
    {


        [Key]
        public int ID { get; set; }

        public int teacher_id { get; set; }
        [ForeignKey("teacher_id")]
        public virtual TeacherProfileTable Teacher { get; set; }


        // separated with semicolons 0;1;2
        // !!! Depreciated
        public string daysAvailable { get; set; }

        // separated with semicolons 0;1;2
        public string daysNotAvailable { get; set; }

        public int week { get; set; }


        public int year { get; set; }


    }
}
    