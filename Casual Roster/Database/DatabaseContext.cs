﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Casual_Roster
{



    internal class DatabaseContext : DbContext
    {
        public DatabaseContext()
            : base("name=DatabaseContext")
        { }

        public DbSet<UserTable> Users { get; set; }
        public DbSet<TeacherProfileTable> TeacherProfiles { get; set; }
        public DbSet<SchoolProfileTable> SchoolProfiles { get; set; }
        public DbSet<TeacherAvailability> TeacherAvailabilites { get; set; }
        public DbSet<RosterRequest> RosterRequests { get; set; }
        public DbSet<SchoolTeacherRequest> SchoolTeacherRequests { get; set; }
        public DbSet<ImageTable> Images { get; set; }
        public DbSet<LocationTable> Locations { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);



            // RosterRequests table
            modelBuilder.Entity<TeacherProfileTable>()
                .HasMany(c => c.RosterRequests)
                .WithRequired()
                .HasForeignKey(c => c.teacher_id);

            modelBuilder.Entity<SchoolProfileTable>()
                .HasMany(c => c.RosterRequests)
                .WithRequired()
                .HasForeignKey(c => c.school_id);

            // SchoolTeacherRequests table
            modelBuilder.Entity<TeacherProfileTable>()
                .HasMany(c => c.SchoolTeacherRequests)
                .WithRequired()
                .HasForeignKey(c => c.teacher_id);

            modelBuilder.Entity<SchoolProfileTable>()
                .HasMany(c => c.SchoolTeacherRequests)
                .WithRequired()
                .HasForeignKey(c => c.school_id);

            

            // coordinates
            modelBuilder.Entity<LocationTable>().Property(l => l.lat).HasPrecision(8, 5);
            modelBuilder.Entity<LocationTable>().Property(l => l.lng).HasPrecision(8, 5);


        }

    }




}
