﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.Data.Entity;

namespace Casual_Roster
{
    public class Roster
    {

        public int id { get; set; }
        Teacher teacher;
        School school;
        DateTime rosterDate = new DateTime();


        public Teacher Teacher
        {
            get
            {
                return teacher;
            }
        }

        public School School
        {
            get
            {
                return school;
            }
        }

        public int RosterDateUnix
        {
            get
            {
                return (Int32)this.Request.roster_date.ToUniversalTime().Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
            }
        }
        

        public enum Statuses
        {
            Pending,
            Declined,
            Approved
        }

        public RosterRequest Request
        {
            get
            {
                var db = App.Database;
                if (id == 0 || id == null)
                {
                    int dateYear = this.rosterDate.Year;
                    int dateMonth = this.rosterDate.Month;
                    int dateDate = this.rosterDate.Day;
                    return db.RosterRequests.FirstOrDefault(r =>
                        r.roster_date.Year == dateYear &&
                        r.roster_date.Month == dateMonth &&
                        r.roster_date.Day == dateDate &&
                        r.teacher_id == this.teacher.id &&
                        r.school_id == this.school.id
                    );
                }
                else
                {
                    return db.RosterRequests.FirstOrDefault(r => r.ID == id);
                }
                
            }
        }


        public Roster(Teacher teacher, School school, DateTime rosterDate)
        {
            this.teacher = teacher;
            this.school = school;
            this.rosterDate = rosterDate;
        }

        public Roster(int id)
        {
            var db = App.Database;
            var roster = db.RosterRequests.FirstOrDefault(r => r.ID == id);
            this.teacher = new Teacher(roster.teacher_id);
            this.school = new School(roster.school_id);
            this.rosterDate = roster.roster_date;
            this.id = id;
        }

        public void Save()
        {
            if (teacher == null || school == null || rosterDate < new DateTime())
            {
                // invalid values
                throw new Exception("Invalid data params");
            }
            var db = App.Database;
            var roster = new RosterRequest
            {
                teacher_id = this.teacher.id,
                school_id = this.school.id,
                roster_date = rosterDate,
                created = DateTime.Now,
                status = Roster.Statuses.Pending.ToString()
            };
            db.RosterRequests.Add(roster);
            db.SaveChanges();
            
            // send sms
            var schoolProfile = this.school.Profile;
            var teacherProfile = this.teacher.Profile;
            string message = String.Format(
                "Hi {0}! You have been requested by {1} at {2}. Contact # is {3}. Press this link for confirmation {4}",
                new string[] { 
                    teacherProfile.firstName,
                    schoolProfile.schoolName,
                    this.rosterDate.ToShortDateString(),
                    Utils.FormatMobileNumber(schoolProfile.mobile, schoolProfile.mobileCountryCode, PhoneNumbers.PhoneNumberFormat.E164),
                    Utils.AbsoluteUrl("/Teacher/RosterAction?rosterId=" + roster.ID)
                }
            );
            new SMS(Utils.FormatMobileNumber(teacherProfile.mobile, teacherProfile.mobileCountryCode, PhoneNumbers.PhoneNumberFormat.INTERNATIONAL)).Send(message);
        }

        public static IEnumerable<Roster> GetRequestsForDay(DateTime date, int schoolId = 0, int teacherId = 0) {
            var db = App.Database;
            int dateMonth = date.Month;
            int dateYear = date.Year;
            int dateDate = date.Day;

            IQueryable<RosterRequest> rosters = null;
                
            if(schoolId != 0) {
                rosters = db.RosterRequests.Where(r =>
                    r.roster_date.Month == dateMonth &&
                    r.roster_date.Year == dateYear &&
                    r.roster_date.Day == dateDate &&
                    schoolId == r.school_id
                );
            }

            if (teacherId != 0)
            {
                rosters = db.RosterRequests.Where(r =>
                    r.roster_date.Month == dateMonth &&
                    r.roster_date.Year == dateYear &&
                    r.roster_date.Day == dateDate &&
                    teacherId == r.teacher_id
                );
            }

            if (teacherId == 0 && schoolId == 0)
            {
                rosters = db.RosterRequests.Where(r =>
                   r.roster_date.Month == dateMonth &&
                   r.roster_date.Year == dateYear &&
                   r.roster_date.Day == dateDate
               );
            }


            foreach(var request in rosters) {
                yield return new Roster(request.ID);
            }
        }

        public static IEnumerable<Roster> GetRequestsInWeek(int week, int userId)
        {
            var db = App.Database;
            var requests = db.RosterRequests.Where(r =>
                //CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(r.roster_date, CalendarWeekRule.FirstDay, DayOfWeek.Monday) == week
                System.Data.Entity.SqlServer.SqlFunctions.DatePart("ISO_WEEK", r.roster_date).Value == week &&
                (r.teacher_id == userId || r.school_id == userId)
            );
            foreach (var request in requests)
            {
                yield return new Roster(request.ID);
            }
        }


        public void UpdateStatus(Roster.Statuses status)
        {
            var db = App.Database;
            var roster = db.RosterRequests.FirstOrDefault(r => r.ID == this.id);
            if (roster != null)
            {
                roster.status = status.ToString();

                
                // send sms for notification
                var teacher = db.TeacherProfiles.FirstOrDefault(t => t.user_id == roster.teacher_id);
                var school = db.SchoolProfiles.FirstOrDefault(t => t.user_id == roster.school_id);
                string message;

                if (status == Statuses.Approved)
                {
                    message = String.Format(
                        "Your roster request for {0} has been accepted. Contact # {1}",
                        new string[] { 
                            teacher.firstName,
                            Utils.FormatMobileNumber(teacher.mobile, teacher.mobileCountryCode, PhoneNumbers.PhoneNumberFormat.E164),
                        }
                    );

                }
                else
                {
                        message = String.Format(
                            "Your roster request for {0} has been declined by teacher. Contact # {1}",
                            new string[] { 
                                teacher.firstName,
                                Utils.FormatMobileNumber(teacher.mobile, teacher.mobileCountryCode, PhoneNumbers.PhoneNumberFormat.E164),
                            }
                        );
                }

                // send to school admin
                new SMS(Utils.FormatMobileNumber(school.mobile, school.mobileCountryCode, PhoneNumbers.PhoneNumberFormat.INTERNATIONAL)).Send(message);
         
                db.SaveChanges();
            }
        }


    }
}
