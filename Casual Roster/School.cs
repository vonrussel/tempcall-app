﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using PhoneNumbers;

namespace Casual_Roster
{

    public class School
    {

        public static List<String> Industries = new List<string>()
        {
            "Education",
            "Hospitality",
            "Transport",
            "Construction",
            "Agriculture",
            "Tourism",
            "Government",
            "Finance",
            "Retail",
            "Wholesale"
        };

        public int id { get; set; }


        public School(int userId)
        {
            this.id = userId;
        }

        public SchoolProfileTable Profile
        {
            get
            {
                var db = App.Database;
                return db.SchoolProfiles.FirstOrDefault(s => s.user_id == this.id); 
            }
        }

        public Image Logo
        {
            get
            {
                if (this.Profile.logo != null)
                {
                    return new Image((int)this.Profile.logo);
                }
                return null;
            }
        }

        public void UpdateLogo(HttpPostedFileBase image, HttpServerUtilityBase server)
        {
            var logo = new Image(image);
            var uploadedLogo = logo.Save(server);
            var db = App.Database;
            var self = db.SchoolProfiles.FirstOrDefault(s => s.user_id == this.id);
            self.logo = uploadedLogo.ID;
            db.SaveChanges();
        }

        public void UpdateProfile(SchoolProfileTable profile)
        {
            var db = App.Database;
            var phoneUtil = PhoneNumberUtil.GetInstance();
            var school = db.SchoolProfiles.FirstOrDefault(s => s.user_id == this.id);
            if (school != null)
            {
                school.schoolName = profile.schoolName;
                school.email = profile.email;
                school.postCode = profile.postCode;

                // phone number
                school.mobile = Utils.FormatMobileNumber(profile.mobile, profile.mobileCountryCode);
                school.phones = profile.phones;
                school.mobileCountryCode = profile.mobileCountryCode;

                if (school.location_id != null)
                {
                    // update existing loc data
                    var loc = db.Locations.FirstOrDefault(l => l.ID == school.location_id);
                    loc.lat = profile.Location.lat;
                    loc.lng = profile.Location.lng;
                    loc.formattedAddress = profile.Location.formattedAddress;
                }
                else
                {
                    school.Location = profile.Location; // insert new 
                }

                db.SaveChanges();
            }
        }






        public static IEnumerable<School> GetAvailableSchoolsForTeacher(int teacherId)
        {
            var db = App.Database;
            var teacher = new Teacher(teacherId).Profile;
            var requestedSchoolIds = Array.ConvertAll(db.SchoolTeacherRequests.Where(r => r.teacher_id == teacherId).ToArray(), (r) => r.school_id);

            // limit postCode
            var schools = db.SchoolProfiles.Where(
                s => 
                    s.accountType == AccountTypes.Type.Premium.ToString() || // if premium
                    teacher.postCode == s.postCode ||
                    requestedSchoolIds.Contains(s.user_id) // check if current teacher is requested by this school
            ).Select(c => new { c.user_id });

            foreach (var school in schools)
            {
                yield return new School(school.user_id);
            }
        }


        public static void Register(SchoolProfileTable school)
        {
            // TODO validate here, maybe?
            // this is called at the mvc action so this should be validated
            var db = App.Database;
            var phoneUtil = PhoneNumberUtil.GetInstance();

            // add additional fields
            school.User.HashPassword(20);
            school.User.username = "";
            school.User.roles = "school";
            school.accountType = AccountTypes.Type.Free.ToString(); // default account type is free

            // phone number
            school.mobile = Utils.FormatMobileNumber(school.mobile, school.mobileCountryCode);
            school.phones = school.phones;

            db.SchoolProfiles.Add(school);
            db.SaveChanges();

            Email.SendWelcomeEmail(new MailAddress(school.email, school.schoolName));
        }


        public static void RequestTeacher(TeacherProfileTable teacher, int schoolId)
        {
            Teacher.Register(teacher);
            School.RequestExistingTeacher(teacher.user_id, schoolId, newTeacherId: teacher.user_id);
        }

        public static void RequestExistingTeacher(int teacherId, int schoolId, int newTeacherId = 0)
        {
            var db = App.Database;
         
            // insert to db
            var request = new SchoolTeacherRequest()
            {
                teacher_id = teacherId,
                school_id = schoolId,
                created = DateTime.Now,
                has_registered = newTeacherId > 0
            };
            db.SchoolTeacherRequests.Add(request);
            db.SaveChanges();

            // send sms to teacher
            var school = db.SchoolProfiles.FirstOrDefault(s => s.user_id == schoolId);
            var teacher = db.TeacherProfiles.FirstOrDefault(s => s.user_id == teacherId);
            var recipient = Utils.FormatMobileNumber(school.mobile, school.mobileCountryCode, PhoneNumberFormat.INTERNATIONAL);
            var sms = new SMS(recipient);
            // compose sms message
            string message = "Hi {0}! You have been requested by {1} to join Casual Roster Project.";
            // check if this is new teacher
            if (newTeacherId > 0)
            {
                message = String.Format(
                     "Hi {0}! You have been requested by {1} to work with them as a casual Staff. Click this link to continue to register your account {2}",
                     new string[] { 
                        teacher.firstName,
                        school.schoolName,
                        Utils.AbsoluteUrl("/Teacher/RegisterRequested?id=" + newTeacherId)
                    }
                 );
            }
            else
            {
                message = String.Format(
                    "Hi {0}! You have been requested by {1} to work with them as a casual Staff.",
                    new string[] { 
                        teacher.firstName,
                        school.schoolName
                    }
                );
            }
            sms.Send(message);
        }

        public static SchoolTeacherRequest GetRequestTeacherData(int teacherId)
        {
            var db = App.Database;
            return db.SchoolTeacherRequests.FirstOrDefault(s => s.teacher_id == teacherId);
        }






    }


}
