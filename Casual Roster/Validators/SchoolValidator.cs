﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;
using FluentValidation.Results;

namespace Casual_Roster.Validators
{
    public class SchoolValidator : AbstractValidator<SchoolProfileTable>
    {
        public SchoolValidator()
        {
            /* Form validations */
            RuleSet("RegisterSchool", () =>
            {
                RuleFor(x => x.schoolName).NotEmpty();
                RuleFor(x => x.address).NotEmpty();
                RuleFor(x => x.phones).NotEmpty();
                RuleFor(x => x.mobile).NotEmpty(); // TODO: use libphonenumber-csharp
                RuleFor(x => x.email).NotEmpty().EmailAddress();
                RuleFor(x => x.User).SetValidator(new UserValidator());

                Custom(x =>
                {
                    bool existing = User.EmailExists(x.email ?? "");
                    if (existing)
                        return new ValidationFailure("email", "Email address is already in use");
                    return null;
                });

                // check if mobile is valid
                Custom(x =>
                {
                    bool valid = User.mobileIsValid(x.mobile, x.mobileCountryCode);
                    if (!valid)
                        return new ValidationFailure("mobile", "Mobile number is invalid. Do not include country code.");
                    return null;
                });

                // check if mobile exists
                Custom(x =>
                {
                    bool existing = User.mobileExists(x.mobile, x.mobileCountryCode);
                    if (existing)
                        return new ValidationFailure("mobile", "Mobile number is already in use.");
                    return null;
                });



            });
        }
    }


}
