﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;
using FluentValidation.Results;

namespace Casual_Roster.Validators
{
    public class TeacherValidator : AbstractValidator<TeacherProfileTable>
    {
        public TeacherValidator()
        {
            /* Form validations */
            RuleSet("RegisterTeacher", () =>
            {
                RuleFor(x => x.firstName).NotEmpty();
                RuleFor(x => x.lastName).NotEmpty();
                RuleFor(x => x.email).NotEmpty().EmailAddress();
                RuleFor(x => x.mobile).NotEmpty();
                RuleFor(x => x.category).NotEmpty();
                RuleFor(x => x.postCode).NotEmpty();
                RuleFor(x => x.gender).NotEmpty();

                Custom(x =>
                {
                    bool existing = User.EmailExists(x.email);
                    if (existing)
                        return new ValidationFailure("email", "Email address is already in use");
                    return null;
                });

                // check if mobile is valid
                Custom(x =>
                {
                    bool valid = User.mobileIsValid(x.mobile, x.mobileCountryCode);
                    if (!valid)
                        return new ValidationFailure("mobile", "Mobile number is invalid. Do not include country code.");
                    return null;
                });

                // check if mobile exists
                Custom(x =>
                {
                    bool existing = User.mobileExists(x.mobile, x.mobileCountryCode);
                    if (existing)
                        return new ValidationFailure("mobile", "Mobile number is already in use.");
                    return null;
                });
            });

         

            

        }
    }


}
