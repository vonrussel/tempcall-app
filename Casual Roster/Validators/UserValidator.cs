﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;
using FluentValidation.Results;

namespace Casual_Roster.Validators
{
    public class UserValidator : AbstractValidator<UserTable>
    {

        public UserValidator()
        {
            /* Form validations */

            // defaults
            RuleFor(x => x.username).NotNull();
            RuleFor(x => x.password).NotNull();

            // executes when school or teacher registers
            RuleSet("RegisterSchool", () =>
            {
                RuleFor(x => x.password).NotNull();
            });
            RuleSet("RegisterTeacher", () =>
            {
                RuleFor(x => x.username)
                    .Matches(@"^(?=.{5,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$")
                    .WithMessage("Username must be in correct format");


                // check if username exists
                Custom(x =>
                {
                    bool existing = User.UsernameExists(x.username);
                    if (existing)
                        return new ValidationFailure("username", "Username is already in use");
                    return null;
                });


            });

            RuleSet("RegisterRequested", () =>
            {
                // check if username exists
                Custom(x =>
                {
                    x.username = x.username ?? "";

                    bool existing = User.UsernameExists(x.username, x.ID);
                    if (existing)
                        return new ValidationFailure("username", "Username is already in use");
                    return null;
                });
            });






            // login
            RuleSet("LoginWithEmail", () =>
            {
                Custom(x =>
                {
                    return User.Auth(x.username, x.password) ? null :
                      new ValidationFailure("username", "Email / Password does not match");

                });
            });


        }



    }
}

