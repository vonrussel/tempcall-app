﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.Net.Mail;
using System.Web;
using PhoneNumbers;

namespace Casual_Roster
{
    public class Teacher
    {

        public static List<String> Categories = new List<string>()
        {
            "Teacher", 
            "Kitchen Hand",
            "Cleaner", 
            "Housekeeping", 
            "Fruit Picker", 
            "Retail Assistant", 
            "Warehouse person", 
            "Brick layer", 
            "Truck driver"
        };
        
        public int id { get; set; }

        public Teacher(int id) {
            this.id = id;
        }

        public TeacherProfileTable Profile 
        { 
            get {
                var db = App.Database;
                return db.TeacherProfiles.FirstOrDefault(t => t.user_id == id);
            }
        }


        public static IEnumerable<Teacher> GetAvailableIn(DateTime date, int schoolId = 0)
        {
            // get what day is requested
            int day = (int)CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(date);
            int week = CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(date, CalendarWeekRule.FirstDay, DayOfWeek.Monday);
            int year = date.Year;
            // set monday first not sunday
            day = day == 0 ? 6 : day - 1;
            
            // get who is available
            var db = App.Database;
            string[] teacherIds = new string[] {};
            string dateString = date.ToString("yyyy-mm-dd");
            if (schoolId != 0)
            {
                // exclude teacheres who are already requested
                var q =
                    db.RosterRequests
                        .Where(r => r.school_id == schoolId)
                        .Select(c => new { c.teacher_id, c.roster_date });
                teacherIds = Array.ConvertAll(q.ToArray(), cols => cols.teacher_id + ";" + cols.roster_date.ToString("yyyy-mm-dd") );
            }

            var availables = db.TeacherAvailabilites.Where(a =>
                a.year == year && a.week == week && !a.daysNotAvailable.Contains(day.ToString()) &&
                !teacherIds.Contains(a.teacher_id + ";" + dateString)
            );

            foreach (var available in availables)
            {
                yield return new Teacher(available.teacher_id);
            }
        }

        public static IEnumerable<Teacher> GetAll()
        {
            var db = App.Database;
            var teachers = db.TeacherProfiles.Where(t => t.user_id != null);
            foreach (var teacher in teachers)
            {
                yield return new Teacher(teacher.user_id);
            }
        }

        public int[] GetAvailabilities(int week, int year)
        {
            var db = App.Database;
            var availabilities = db.TeacherAvailabilites.FirstOrDefault(t => 
                t.teacher_id == this.id &&
                t.week == week &&
                t.year == year
            );
            var days = new List<int>() { 0, 1, 2, 3, 4, 5, 6 };

            if (availabilities != null)
            {
                var arr = (availabilities.daysNotAvailable ?? "").Split(';');
                var daysNotAvailable = new List<int>() { }; 

                foreach (var day in days)
                {
                    if (!arr.Contains(day + ""))
                    {
                        daysNotAvailable.Add(day);
                    }
                }
                return daysNotAvailable.ToArray();
            }
            return days.ToArray();
        }

        public void SaveAvailabilities(int[] availabilities, int week, int year)
        {
            if (availabilities == null)
            {
                availabilities = new int[] { };
            }
            // negate availabilities
            // save what dates teacher arent available
            var notAvailable = new List<int>() { 0, 1, 2, 3, 4, 5, 6 };
            foreach(var a in availabilities) {
                notAvailable.Remove(a);
            }

            string strNotAvailable = String.Join(";", notAvailable.ToArray());
            var db = App.Database;
            var existing = db.TeacherAvailabilites.FirstOrDefault(a => 
                a.teacher_id == this.id &&
                a.year == year &&
                a.week == week
            );
            
            if (existing != null)
            {
                existing.daysNotAvailable = strNotAvailable;

            } else {
                db.TeacherAvailabilites.Add(new TeacherAvailability
                {
                    teacher_id = this.id,
                    daysNotAvailable = strNotAvailable,
                    week = week,
                    year = year
                });
            }
            db.SaveChanges();
        }

        public void UpdateProfile(TeacherProfileTable profile)
        {
            var db = App.Database;
            var teacher = db.TeacherProfiles.FirstOrDefault(t => t.user_id == profile.user_id);
            if (teacher != null)
            {

                // phone number
                teacher.mobileCountryCode = profile.mobileCountryCode;
                teacher.mobile = Utils.FormatMobileNumber(profile.mobile, profile.mobileCountryCode);
                teacher.mobileAlter = profile.mobileAlter;

                // update only specified fields
                teacher.firstName = profile.firstName;
                teacher.lastName = profile.lastName;
                teacher.email = profile.email;
                teacher.postCode = profile.postCode;
                teacher.category = profile.category;
                teacher.gender = profile.gender;
                teacher.preferredSchools = profile.preferredSchools;

                if (teacher.location_id != null)
                {
                    // update existing loc data
                    var loc = db.Locations.FirstOrDefault(l => l.ID == teacher.location_id);
                    loc.lat = profile.Location.lat;
                    loc.lng = profile.Location.lng;
                    loc.formattedAddress = profile.Location.formattedAddress;
                }
                else
                {
                    teacher.Location = profile.Location; // insert new 
                }

                db.SaveChanges();
            }
        }

        public void UpdatePhoto(HttpPostedFileBase image, HttpServerUtilityBase server)
        {
            var logo = new Image(image);
            var uploadedLogo = logo.Save(server);
            var db = App.Database;
            var self = db.TeacherProfiles.FirstOrDefault(s => s.user_id == this.id);
            self.photo = uploadedLogo.ID;
            db.SaveChanges();
        }


        public static TeacherProfileTable GetByMobile(string mobile, string countryCode)
        {
            // clean and format mobile
            mobile = mobile ?? "";
            mobile = Utils.FormatMobileNumber(mobile, countryCode);

            var db = App.Database;
            return db.TeacherProfiles.FirstOrDefault(t => mobile == t.mobile && t.mobileCountryCode == countryCode );
        }


        // static methods

        public static void Register(TeacherProfileTable teacher)
        {
            // TODO validate here, maybe?
            // this is called at the mvc action so this should be validated
            var db = App.Database;

            if (teacher.User != null)
            {
                // add additional fields
                if(!String.IsNullOrEmpty(teacher.User.password))
                    teacher.User.HashPassword(20);
                teacher.User.roles = "teacher";
            }

            // phone number
            teacher.mobile = Utils.FormatMobileNumber(teacher.mobile, teacher.mobileCountryCode);
            teacher.mobileAlter = teacher.mobileAlter;

            db.TeacherProfiles.Add(teacher);
            db.SaveChanges();

            Email.SendWelcomeEmail(new MailAddress(teacher.email, teacher.firstName));
        }

        public static void UpdateRequested(TeacherProfileTable teacher)
        {
            var db = App.Database;
            var _teacher = db.TeacherProfiles.FirstOrDefault(t => t.user_id == teacher.user_id);
            var request = db.SchoolTeacherRequests.FirstOrDefault(t => t.teacher_id == teacher.user_id);
            var phoneUtil = PhoneNumberUtil.GetInstance();

            if (request != null)
            {

                // phone number
                _teacher.mobileCountryCode = teacher.mobileCountryCode;
                _teacher.mobile = Utils.FormatMobileNumber(teacher.mobile, teacher.mobileCountryCode);
                _teacher.mobileAlter = teacher.mobileAlter;

                _teacher.User.username = teacher.User.username;
                _teacher.User.password = teacher.User.password;
                _teacher.firstName = teacher.firstName;
                _teacher.lastName = teacher.lastName;
                _teacher.email = teacher.email;
                _teacher.gender = teacher.gender;
                // exclude category
                _teacher.User.HashPassword(20);

                // update request table
                request.has_registered = true;

                db.SaveChanges();
                return;
            }

            // throw exception
        }

        





    }
}
