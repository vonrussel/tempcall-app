﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Stripe;

namespace Casual_Roster
{
    public class Payments
    {
        // https://github.com/jaymedavis/stripe.net


        public static void InitializeStripe()
        {
            StripeConfiguration.SetApiKey("sk_test_nyeQTjC2jHkE98z2zdu6iZUB");
        }


        public IEnumerable<StripePlan> GetAllPlans()
        {
            var planService = new StripePlanService();
            return planService.List();
        }

        public static void RegisterCustomer(UserTable userTable, StripeCard card = null)
        {
            var customer = new StripeCustomerCreateOptions();
            var user = new User(userTable.ID);

            // set these properties if it makes you happy
            
            if (user.IsInRole(Roles.Option.school))
            {
                customer.Email = user.SchoolProfile.email;
                customer.Description = String.Format("{0} ({1})",
                    user.SchoolProfile.schoolName,
                    customer.Email
                );
            }
            else
            {
                customer.Email = user.TeacherProfile.email;
                customer.Description = String.Format("{0} ({1})",
                    user.TeacherProfile.firstName + " " + user.TeacherProfile.lastName,
                    customer.Email
                );
            }

            if (card != null)
            {
                customer.Source = new StripeSourceOptions()
                {
                    // set this property if using a token
                    // TokenId = *tokenId*,

                    // set these properties if passing full card details (do not
                    // set these properties if you set TokenId)
                    Number = card.AccountId,
                    ExpirationYear = card.ExpirationYear,
                    ExpirationMonth = card.ExpirationYear,
                    AddressCountry = card.Country,
                    AddressLine1 = card.AddressLine1,
                    AddressLine2 = card.AddressLine2,
                    AddressCity = card.AddressCity,
                    AddressState = card.AddressState,
                    AddressZip = card.AddressZip,
                    Name = card.Name,
                    Cvc = card.CvcCheck
                };
            }

            var customerService = new StripeCustomerService();
            StripeCustomer stripeCustomer = customerService.Create(customer);
            User.UpdateStripeCustomerId(user.ID, stripeCustomer.Id);

        }

        public void UpdateUserCard(UserTable userTable, StripeCard card)
        {
            if (String.IsNullOrEmpty(userTable.stripeCustomerId))
            {
                throw new Exception("User must have a stripeCustomerId first");
            }

            var customer = new StripeCustomerUpdateOptions();
            var user = new User(userTable.ID);

            if (card != null)
            {
                customer.Source = new StripeSourceOptions()
                {
                    // set this property if using a token
                    // TokenId = *tokenId*,

                    // set these properties if passing full card details (do not
                    // set these properties if you set TokenId)
                    Number = card.AccountId,
                    ExpirationYear = card.ExpirationYear,
                    ExpirationMonth = card.ExpirationYear,
                    AddressCountry = card.Country,
                    AddressLine1 = card.AddressLine1,
                    AddressLine2 = card.AddressLine2,
                    AddressCity = card.AddressCity,
                    AddressState = card.AddressState,
                    AddressZip = card.AddressZip,
                    Name = card.Name,
                    Cvc = card.CvcCheck
                };
            }

            var customerService = new StripeCustomerService();
            StripeCustomer stripeCustomer = customerService.Update(user.stripeCustomerId, customer);
            
        }
       
    }

}
